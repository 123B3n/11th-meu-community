Welcome to 11th MEU's documentation!
######################################

This documentation covers rules and features of the `11th Marine Expeditionary Unit Community`_.
This resource uses both markdown and Sphinx in .txt documents.
Use the side bar for navigation.

.. _11th Marine Expeditionary Unit Community: https://11th-meu.org/
.. _Forums: https://forums.11th-meu.org/
.. _github: https://gitlab.com/11th-meu

.. toctree::
   :maxdepth: 2
   :caption: General Rules:

   rules/overview
   rules/forums
   rules/arma
   
.. toctree::
   :maxdepth: 2
   :caption: Sunrise Roleplay:

   rules/intro
   rules/server-rules

Links
==================

* `Forums`_
* `Github`_
