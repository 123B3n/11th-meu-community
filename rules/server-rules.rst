#############
Server Rules
#############


1.0 Civility
------------
This can be otherwise stated as, "play nice" It should cover every sort of mean-spirited thing that players can do to each other. We are all here to have fun, and each and every player should try and think before they say or do certain things.

Do not engage in name-calling, bad attitudes, OOC insults, racism, hate speech, spam, excessive caps lock, griefing, trolling, provoking, harassment, insulting each other in different languages, threatening etc. Apologize if you accidentally lag-P.I.T someone or disrupt their role play because of an OOC mistake. Be courteous and fair when playing here.

**Examples:**

+ Ben calls Jeff an "idiota" in out of character chat, knowing that the other players around do not speak Spanish.
+ Ben parks his vehicle on Jeff's vehicle spawn point, knowing he might be able to flip Jeff's car if he spawns it.
+ Ben constantly tells people to "screw off" in OOC chat because he is having a bad day.
+ Ben is having internet issues and accidentally lags into a group of role players. He just says "lol" in OOC chat before driving away.
+ Ben decides it would be funny to RP masturbating in a public place to annoy the other players around him.
+ Ben drives up to a group of role players and calls them racial slurs in an attempt to get them to chase him.


1.1 Chat
--------
All chatting functions are in character unless bracketed with double parenthesis like this - (( Chat )). All OOC chat should be relegated to /b ('b' key) and /pm ('u' key). Do not apply brackets to any of the in character chatting methods, including radios, cellphones, megaphones, averts, SAN News and News broadcasts, etc.

**Example:**

+ Ben approaches Jeff and says, "Hey, I am looking for some toys (( Guns ))."
+ Do not use any internet shortcuts (such as lol, btw, gg, qt, smiley faces) while typing In Character chat. The only exception is while using the in game cellphone text message/SMS function.
+ An example of improper usage: Ben asks Jeff, "how 2 find the DMV lol."
+ An example of proper usage: Jeff uses the SMS cellphone function, "Hey Ben , where r u?"
+ All IC & OOC (unless in PM) chat must be in English. Slang may be used in the case of multilingual characters, but full non-english sentences are not permitted. To roleplay speaking another language, use the language itself which you can learn in a library, however even if you speak in the Spanish language you should still speak English as your character may be Spanish, however, everyone doesn't know Spanish. None or less the languages that your character doesn't know are scrambled so that other players won't understand it unless they know the same language as you.
+ Example: Me gusta Los Santos mucho! should be, "[Spanish] I like Los Santos a lot!"
+ Slang example: What's up, amigo?


1.2 Deathmatch
--------------
Deathmatch is the act of killing another player for little to no reason. Killing is a serious subject and your role play should reflect that.

This includes Revenge Killing which is when you attempt to kill a player who has recently killed you. Your memory of recent events is wiped out upon death, so you are unable to remember who killed you. However, of the LSFD manages to save you from dying you are able to carry out the revenge role play.

You are also not allowed to kill or attack any on-duty LSFD member.

You may not take assassination contracts or "hits" from other players. Receiving payment solely for the act of killing another person is prohibited. Money must not be a major factor in your decision to kill someone. If you wish to role play a hitman or assassin (or create a faction of them) you must acquire lead admins approval.

If you are caught death matching by an admin and you are admin jailed the admin has the ability to request or have your weapon license removed and can ban you from obtaining a weapon for a certain amount of time.

**Example:**

+ Ben pulls out a gun and shoots Jeff because he is bored.
+ Ben is a crip and Jeff is a blood. Even though the two characters have never had any interaction, Ben decides to kill Jeff for being a blood.
+ A man approaches Ben and pays him 1000$ to kill Jeff. Ben accepts the contract and assassinates Jeff despite having no prior role play with either player.


1.3 Metagaming
--------------
Metagaming is defined as using out of character information for in character purposes. Your character must discover and learn all pertinent information via in character sources.

Voice over programs such as team speak and Skype are considered OOC chat, and using them as IC methods (for example, using team speak as if it's a cellphone) is prohibited for all players. The exception to this rule is Law Enforcement factions.

**Example:**

+ Ben sees Jeff's name above his head and role plays knowing his name.
+ Jeff tells Ben over TeamSpeak that he is being robbed at the airport. Jeff metagaming in this case. If Ben shows up to help, he is also metagaming.
+ Ben is being questioned about a crime, and Ben tells the cops all of the details about the crime because he saw everything in a screenshot on the forums.
+ Ben PMs Jeff saying, "I am going to kill you when you come outside your house." Jeff is metagaming if he then alters his role play according to what he just learned in OOC chat.


1.4 Powergaming
---------------
Power gaming covers three different parts of RP; role playing things that are impossible or unrealistic, forcing your role play onto another person, or making up RP situations for the benefit of your character without any of the negative consequences.

**Examples:**

+ Ben attempts to role play being a ninja. When another player attempts to attack him, he does "/me does a backflip to dodge the punch and leaps over a nearby building to safety."
+ Ben approaches Jeff and types, "/me quickly grabs Jeff's neck and instantly chokes him to death."
+ Cops are searching Ben and find a gun on him. He explains that he doesn't actually have the gun on him and he "RP" stored it at his house earlier.**
+ Ben role plays as a police officer. He uses /do to role play an NPC civilian who tells him pertinent information about a murder case he is working on. He then arrests a player according to what the NPC he created told him.
**You cannot** "RP" store items somewhere else and keep them on your person. You must actually use the appropriate script-storage.


1.5 Always Role play Realistically
----------------------------------
Except for admins on duty, every player must be "In Character" at all times. Supporters on-duty are still expected to be "In-Character." There is no 'going OOC' unless an admin has requested it. Do not stop or slow down role play, attempt to stall, or use excessive /b ('b' key) or other OOC chat, and do not use certain script functions to replace your role play. Attempt to make your role play as realistic as possible while in game.

**This rule includes, but is not limited to:**

+ Using /quit or force crash to avoid role play
+ "Ninja Jacking" or using no /me when pulling another player from their vehicle
+ Running someone over without proper use of /me or /do
+ Unrealistic vehicle usage, such as doing 140 KMH on city streets, bicycle jumping onto buildings, doing insane or unrealistic motorcycle jumps during police chases, or avoiding car crash RP with simple things like "/do Saved by Seat belt"
+ Placing unrealistic advertisements (such as "selling a book about eagles with 50 pages")
+ RPing large weapons in small bags (shotgun in a backpack, AK in a suitcase, etc.)
+ Excessive swimming with no fatigue or other negative effects
+ Killing yourself to avoid role play
**You are allowed** to use the "respawn" when people are roleplaying with your body unless those players are part of the LSPD or LSFD.


1.6 Hacking, Cheating, Bug abusing, and exploits
------------------------------------------------
Third party modifications that give your character unfair advantages are forbidden to use on this server, and players should not have them installed or in use when connecting to the game mode. Examples of this include health hacks, armor hacks, aimbot, speed hacks, and scripts to avoid the AFK timers. Using skin mods that do not resemble the original skin (such as making normal skins very small to avoid being hit as easy during gun fights) is an exploit and also forbidden. Abusing server bugs for personal gain is, again, forbidden.

You are required to report other players who you know are bug abusing, cheating, hacking, or exploiting. They ruin the experience for everyone involved, and not reporting them will cause you to face punishment as well. Always report bugs to the bug report section.

**You are not allowed** to disturb or provide any information on how to get cheats.


1.7 Abusing GTA:SA physics
--------------------------
Do not abuse game physics in order to give yourself an advantage. This most commonly applies to bunny hopping (repeatedly jumping to go faster), but also covers the C-bug (crouching and un crouching rapidly to shoot faster), G-bugging into a locked car, hiding from pursuers in non-solid houses or broken seams in the game world, stored items to heal yourself during an active fight, W-tapping on motorcycles, abusing house mapping, car parking, vehicle surfing**, and using the instant knife kill**.

**You are not allowed** to car surf on vehicles and "RP" there is an extra seat in the vehicle (or that you are in the trunk). You are allowed to sit in the back of pickup trucks and boats to ride and use weapons. You may have one person surfing on a bike RPing as if he was riding on the bike pegs.


1.9 /me, /do, and /ame
----------------------
Roleplay centers around the commands of /me, /do, and /ame. "/me" is used to explain your characters actions, such as "/me claps his hands." /do is used to describe a scene of role play, and may include your character's part in that scene, such as "/do Jeff sits in the dark room and counts out the money on the table." /ame only makes the text appear above your character's head, and is typically used for minor instances of roleplay such as clothing descriptions or minor character ticks, like "/me coughs into his hands." Never use /ame to describe actions against another player or taking out weapons.

You have to use one of these commands to describe everything you do, especially for things like taking out weapons or attacking another player. The way you type and describe things is the backbone of role play, so make sure to really pay attention to how you type and word your /me, /do, and /ame commands. While not everything requires the use of one of these commands (such as running or driving) it is considered common role playing courtesy to provide a /me or a /do to explain when your character does something.

Never compound too many actions into a single /me. Make sure your /me and /do commands are fair for everyone involved. Doing something like, "/me stands up and dusts himself off" is a fair /me, whereas, "/me stands up and dusts himself off and then quickly sprints away before hopping the fence" is not. Make sure your descriptions and role play are fair for all parties involved. Remember to continue to role play even if you feel the other party is being unfair, and take screenshots and report the incident to the admins.

/do is NOT the medium to contest things that happen in role play. Never do things like "/do I was too far away lol, you couldn't hit me" or "/do I did, I will report you if you don't role play back." If you need clarification on something that happened in an RP situation, attempt to contact the other party via OOC chat. Using Internet shortcuts in /me, /do, or /ame is still against the rules.

**Something to note** if weapons are attached with /togattach you are no longer required to use a /me command stating the (un)holster progress, however, keep in mind that the weapon must be visible or else you have to still do the /me command. This rule is brand new and aren't very common on most roleplay servers.


1.9 Interaction with Admins
---------------------------
Admins are volunteers that have been appointed by management to help enforce the rules and resolve issues that may arise. Feel free to private message admins at any time with questions, comments, or concerns. But also understand that they are typically pretty busy, and may not be able to respond to you at all times. Do not use /report ('F2' key) as a medium to talk to an admin or staff member. You may be kicked from the server if this occurs.

You are not allowed to ignore or lie to an admin in OOC chat. You must follow their instructions, and respond to them when they ask. All admin instructions are final whether or not they are stated in the rules. They can void role play, ask for re-dos, and instruct role players to leave an area. Do not insult them or tell them how to do their job. Treat them with respect as you should with any player.

If you feel an admin has abused his or her position, take screenshots and immediately report them in the "Support" section of the forums. If an administrator broke a rule simply use /report or report them at the normal complaint forum like any other player.


2.0 Green zones
---------------
Green zones are the nickname for areas of Los Santos where blatant criminal activity is prohibited. These are areas where law enforcement would be ubiquitous in real life, and committing large obvious crimes there would be immediately halted. It is immersion breaking and unrealistic for crimes to happen in these areas. Alternatively, these areas might be hot spots for new players becoming adjusted to life.. Do not engage in violent role play such as robberies, murder, fist fights, rape, scams, threats, and general harassment at any time while playing in or around the following areas -

+ Perishing Square (including City Hall & The LSPD)
+ The Hospitals
+ On scripted job icons (except pizza stack job)
+ SAN News
 

Temporary Green zones are areas where blatant criminal activity is prohibited during day-time only. 
The following areas are temporary green zones -

+ The Jefferson Church
+ Idlewood Gas, Pizza and Strip Mall Area
+ Ammunition
+ The Vehicle Dealerships
+ The Bank


Night-time criminal activity is allowed in these locations. Night time is between 21:00 to 6:00 IC time.

Since Idlewood Pizza Stack is a job location and the area is considered a temporary zone, the job zone for pizza stack is also considered a temporary zone instead.

In the case that violent roleplay begins in an alternate location and leads to one of the green zoned areas, the roleplay is allowed to continue. It becomes an IC matter for IC law enforcement to handle. These are not meant to be OOC safe-zones for people. The Green zone rule applies to wherever the victim of the crime is when it occurs. For example you cannot stand outside of a green zone and use a long range weapon to kill someone that is standing in the green zone. Non-violent crime, such as bribery, can still occur inside green zones. Green zones include all interiors within the associated areas.


2.1 Robbery, Scams, Theft, and Extortion
----------------------------------------
OOC Scamming, robbery, theft, and extortion is forbidden. You cannot threaten or deceive someone through OOC means in an attempt to gain in character currency from them, because that means you received payment through non-RP methods. You can perform these actions IC, but there are a few limitations:

+ All players involved must be 20 in-game hours or higher.
+ You may not take any more than $5,000 in a robbery and $10,000 in a scam.
+ You may only rob two people in a 24 hour period, this includes anybody involved in the robbing. So if you help a friend rob someone then you also robbed them.
+ You cannot extort (or "tax") players or their businesses/factions for more than $10,000 in a single week.
+ Robberies are violent activities that involve force or intimidation, such as aiming a gun at someone and demanding all of their cash or telling someone to pay you money for you to not hurt their family. Extortion is similar, except recurring payment is required to avoid harm. Scams are dishonest schemes divided to trick someone out of their money, such as offering to take a player's money and invest it but never return with the cash. Theft is stealing items from other people. There is no limit to a number of items, weapons, or drugs you can take from other players. The limits apply only to cash payment.

Please note that killing a player you have just stolen from for "no witnesses" is considered death match. You must have a good reason to execute someone post-robbery, theft, extortion, or scam. You are also not permitted to rob, scam, steal from, or extort on-duty LSFD members.


2.2 Advertising, avoiding punishment, and proxies
-------------------------------------------------
Do not advertise other servers anywhere on Sunrise that includes speaking about them in any way. Do not change your IP in order to ban evade or avoid any other punishment set forth by the staff team. And because of ping and latency matters in this online video game, we see no reason to allow proxies, VPNs, or anonymous IP address to access the game server or forums.

You are allowed to use a VPN or proxy to access the game server if you are over 180 Ping, but only if you are unable to connect to the game server or remain connected to your home network. You are not allowed to use it for privacy reasons.

You must explain why you need to use a VPN or proxy if asked by a staff member,
you are also required to use your original IP on the forums.


2.3 Money Transferring/Farming, Account Exchange, and profiting from goods
--------------------------------------------------------------------------
It is against the rules to money farm in any way, and you may not transfer goods and money between your characters or accounts. You also may not exchange or transfer accounts with anyone, including friends and siblings. None of this is allowed unless you have permission from a lead admin.

It is also forbidden to sell or profit in any way off of Sunrise Roleplay. You cannot trade something in real life for in game vehicles, donation packages, money, specific RP situations, application help, etc.

Your account is your responsibility. Do not allow younger siblings access to your account or password. If they log in and get banned for hacking or rule breaking, it is your fault for not taking better care of your account. 
Change your password if you fear for the security of your account.

**Examples:**

+ Ben has an awesome skin mod that Jeff wants. Ben sells Jeff the skin mod for $5,000 in game cash.
+ Ben is good at filling out the Sunrise RP application, so he creates accounts and sells them to people for money.
+ Ben donates for donation packages and sells them to people for in game cash.


2.4 Explicit Role play
----------------------
Sexual role play such as rape and sexual assault must have prior OOC agreement by the parties involved before the role play continues. Due to the graphic and disturbing nature of this role play, it can sometimes cause real life unease and displeasure, and players are not allowed to spring this kind of graphic content on others without proper warning and consent.

Even after consent is given, either party can revoke their consent and stop the role play if they begin to feel uncomfortable. Explain to the other role player your feelings and work out a way to either skip the scene (such as a simple /do describing what occurred WITHOUT graphic content) or void the role play that occurred.

The following role play is permitted without actual OOC agreement pedophilia, cannibalism, bestiality and sexual torture.


2.5 Away from Keyboard
----------------------
Players are permitted to pause the game (via the ESC button or Alt-Tab) and go "AFK" inside private interiors for as long as they want. It is against the rules to go AFK during any sort of role play or while an admin is attempting to get your attention. You are not allowed to be AFK in a public area for more than 10 minutes or 600 seconds.

AFKing in public or without pausing the game will result in a kick from the server. AFKing in the middle of role play or admin interaction will result in an admin jail. Using third party scripts or holding down your key to move your character to avoid the AFK timer will result in a server ban.


2.6 Non-RP Names
----------------
All players must have realistic names. They must follow the "Firstname Lastname" format.
Players are not allowed to have the following:

+ Famous Names that are widely recognized in popular culture, such as Michael Jackson or James Woods.
+ Joke Names such as Hugh Jass, Piston Yu, or Mike Hock.
+ Popular fictional names such as Walter White, Carl Johnson, or Patrick Star.
+ Historical Figures like George Washington, Martin King, or Winston Churchill.
+ Real names that are typically used to troll, such as naming a character "Dick" instead of Richard.
Players are allowed to use common names that may conflict with these rules as long as those names are not completely seeped in popular culture. For example Will Smith would not be allowed, but John Smith would.


2.7 Rule-breakers
-----------------
Use /report ('F2' key) to report all rule-breakers, or take screenshots and submit a support ticket. Never break rules because someone else did or does, as you will be at fault for the rules you have broken. If you feel something unfair is happening then you must take screenshots and talk to an admin about it.

It is the duty of the players on Sunrise Roleplay to report rule-breaking and poor role play whenever possible. We all have to do our part to keep undesirable players out and make sure this is a place where everyone can enjoy themselves. Players who are found to be regular rule-breakers may find themselves banned from the server no matter how minor their last offense was.

**Examples:**

+ Ben feels like the cops are meta-gaming and have no reason to arrest him, so he decides to use /quit to get away from them.
+ Ben is being punched by someone and he does not know why, so he takes out his gun and kills that player.
+ Ben sees another player using speed hacks while no admins are on. He hops in the hacker's car and rides with him without reporting him while the hacker speeds around the server, despite Ben not hacking himself.


2.8 Role play Standards
-----------------------
Role play standards are rules that are to be standard when it comes to role-playing on our server. Standards include role playing your death, role playing that someone is knocked out in a fight or brawl and not actually killing them, and one action only per role play command when involving another player.

**Examples:**

+ Ben gets killed by a rival gang member and decides because it isn't a LSPD or LSFD member who killed him he doesn't have to RP and decides to accept death without role play. Ben is in the wrong because you must always role play your death.
+ Ben gets killed by Kevin and Ben believes Kevin DMed, so he decides not to role play his death and instead accepts death. Ben is in the wrong for not role-playing at all times no matter what.
+ Kevin gets into a gang brawl against Ben and Ben falls with (/crack) and Kevin decides to kill him anyways knowing that hes knocked out. Kevin is in the wrong for killing a knocked out person.
+ Ben writes a /me pulls out his gun from his waist and aims the gun to Kevin's head and pulls the trigger. Ben is in the wrong for doing two actions towards Kevin.
+ Ben writes a /me grabs his car keys from his pocket and unlocks his vehicle sticking his keys back into his pocket. Ben is okay because the three actions aren't towards another player.


2.9 Gambling / Bet Limitations
------------------------------
We have limitations in place to limit the amount that can be given and earned total with gambling and betting. The reason for the limitations is to prevent the economy of the game server becoming unrealistic which requires us to raise prices.

**Please note!** following rules listed below are still in development, however, should still be followed as a rule!

+ The total max amount a gamble/bet offer can be is 100,000$
+ The total max reward profit 200,000$
+ The total amount someone is allowed to make daily in a 24 hour period from gambling/betting is 200,000$
This means that nobody is allowed to bet or gamble over 100,000 dollars and that they are not allowed to be rewarded no-more than 200,000 dollars. If you win up to 300,000 dollars you can't gamble or bet anymore for 24 hours.


3.0 Vehicle Theft / Property Break Ins
--------------------------------------
Vehicle theft and property break ins, all though property break ins are not as common as vehicle theft there are still some facts and information that you might need to know if you're going to perform one of them.
Here is some useful information that you might want to know related to the subjects.

**Vehicle Thefts:**

+ A vehicle theft can be anything from stealing the rims of a vehicle to stealing the vehicle itself.
+ So what can you take? You are permitted to take everything from a vehicle besides following items: Emergency Lights/Sirens and Vehicle Trackers.
+ Vehicle ownership may NOT be changed to the one who stole it.
**Property Break Ins:**

+ A vehicle theft can be anything from stealing the rims of a vehicle to stealing the vehicle itself.
+ Breaking into some one's property gives you access to anything you can find besides their safe if you wish to enter their safe you may do so by roleplaying. (Does NOT include finding a key or a combination)
+ You may NOT take someone's safe from their property.
+ Property ownership may NOT be changed to the one who broke into it.
You may now know a bit more about thefts and break ins. The most common mistake people intend to do are the fact that they roleplay this without an admin over watching it and just make a report "I broke into this car, give me the keys"
This is wrong, and the guy who did this has to re-do the entire process again while an admin overwatch the roleplay taking place, you may say "Wow, I've got 10 years experience and yet an admin has to overwatch me" Well it's not about you.
While some people make mistakes and some force the roleplay an admin can correct the mistakes. None or less the main reason for the report is that an admin needs to write down anything that was taken from the property in its notes
this is to prevent people from doing a report and say "I lost my car, can you TP it", while most admin would teleport it back to them, that would not be the case if it's flagged as stolen in its notes.

Even if the vehicle is unlocked and the engine is on, you should still report it to an admin if you steal it. Failing to follow any of this rules may result in a punishment decided by the admin who is handling the case.


3.1 Fake 911 Calls / Provoking Police
-------------------------------------
Not going to write to much about this, we added this a long time ago on our server as a test because we got so many fake 911 calls from people being bored and when cops showed up they got sprayed down with bullets like if it was world war II. This is stupid and so frustrating it's not even funny. You can be patrolling the streets for hours, and once you get a decent 911 call you get instantly killed without the tiniest roleplay. We therefore added this rule so that we could control the 911 calls a bit more.

If you want to do a fake 911 call, you have to consult a staff member before hand, that way the 911 call can be controlled and making the roleplay even better. They admins simply just want to ensure that everyone has a fun time on the server. Provoking the cops falls more or less under the same category, but you aren't required to discuss the plan with an admin beforehand if you don't feel like so, although I highly recommend it, however, do not get but hurt and start doing admin reports when a cop ignores you while provoking him. This of course depends on what the situation is, it is likely to happen as people do this more or less every time they see one.

This isn't really as much as a rule as it is a recommendation, simply just think ahead. Would I like it myself if I was a cop and I had been patrolling the streets for hours without action? would this world war II situation be funny then? just think...


3.2 Voice Usage
---------------
Using our voice function are simple, however, for those who are using the voice functions you still have to give the player who is typing in-game the chance to reply back to you. If you should abuse your voice powers in-game our admins have the right to blacklist you from using the functions.

**Usage:**

+ Voice may only be used as in character
+ Voice may NOT be used as a speaker or any way to play sound through VOIP
+ Voice may NOT replace /me, /do, /ame and any form or roleplay. This is a roleplay server, roleplay should always been the number 1 priority
**Proper Usage:**

+ Ben speaks to Shane, Shane has a microphone but he doesn't like to speak as his character doesn't fit his actual voice. Ben waits for the reply back from Shane. Ben replies back to Shane over voice.
+ Ben walks around in Los Santos singing for himself over voice, Shane stops and tells Ben to get a life and stop singing. Ben gets mad at Shane and starts cursing at Shane. Shane takes out a knife and stabs Ben.
**Improper Usage:**

+ Ben speaks to Shane, however, Shane doesn't have the chance to reply back because Ben keeps speaking and doesn't stop.
+ Ben is playing music over his microphone, while he roleplay playing the music over his phone.
+ Ben speaks, however, it's all OOC information. (Not included in admin situations)
Overall, the voice in-game is built to enhance the roleplay. If anyone is found abusing it, they'll be blacklisted and never be able to use it again. There will be no warnings, before hand. It's a one time deal.