#############
Forum Rules
#############


1.0 General Misconduct
==================
Do not troll, flame, spam, post meaningless content such as single word or off-topic replies, melodramatic or bewailing content or participate in otherwise disruptive behavior. 

Constructive criticism and rational or mature discussion which may illicit an overactive response does not mean the person making the valid point is trolling or flaming. Insulting someone because of a post they made is flaming, telling them you disagree because of a certain reason is not. Toxic behavior expressed by creating drama, or continuous negative posts without room for meaningful discussion is not allowed.


1.1 Illegal Content
===============
Do not post material or links to content which is illegal as per Europe, and International law.


1.2 Advertising
===========
Do not advertise competitors of the 11th Marine Expeditionary Unit, no matter if their friendly or not. If we have a server & they have a server with the same concept or game, said community becomes a competitor by default as we both strive for the same goal.


1.3 English
=======
Do not post on public sections in anything other than legible English.


1.4 Inappropriate Content
=====================
Do not post nudity, gore, or otherwise inappropriate content without spoilers, warnings, and a reason. For example, illegal role-players discussing cartels in real life and showing a video of a shootout would be acceptable, showing random gore for shock value is not.


1.5 Deleting Content
===============
Deleting faction content such as stories, rules, etc, will not occur unless requested by the faction leader.

Posts made on a thread will not be deleted at the request of the thread creator unless it breaks the forum rules. We are not your personal moderators. An exception to this is in-character sections which have are moderated such as website comments.


1.6 Signatures
==========
Signatures shall be non-intrusive and small. No moving images or embedded videos.


1.7 Plagiarism
==========
Do not utilize someone else's personal content without their permission or pass it off as your own.


1.8 Doxing
=======
Do not research or broadcast, personal or identifiable information about an individual without explicit permission.


*Forum administrators may issue warnings, infractions, or bans at their own discretion for breaking the forum rules. You may appeal all punishments at our ticket center* `HERE. <https://support.11th-meu.org/>`_ *Content and works submitted by posting, submission, file uploads, and file links, which were created or posted by you, may be used, modified, deleted, etc, at the discretion of the 11th Marine Expeditionary Unit.*
