# 11th Marine Expeditionary Unit Documentation ![](https://readthedocs.org/projects/11th-meu-community/badge/?version=latest)

The 11th Marine Expeditionary Unit Documentation.

This can be found at https://docs.11th-meu.org/

If working on locally you can build the environment by using `sphinx-autobuild`

### Windows

```
pip install -r requirements.txt
pip install sphinx-autobuild
make html
```

### Linux/MacOS

```
pip install -r requirements.txt
pip install sphinx-autobuild
./Makefile html
```
